#include "ske.h"
#include "prf.h"
#include <openssl/sha.h>
#include <openssl/evp.h>
#include <openssl/hmac.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h> /* memcpy */
#include <errno.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/mman.h>
#ifdef LINUX
#define MMAP_SEQ MAP_PRIVATE|MAP_POPULATE
#else
#define MMAP_SEQ MAP_PRIVATE
#endif

/* NOTE: since we use counter mode, we don't need padding, as the
 * ciphertext length will be the same as that of the plaintext.
 * Here's the message format we'll use for the ciphertext:
 * +------------+--------------------+-------------------------------+
 * | 16 byte IV | C = AES(plaintext) | HMAC(C) (32 bytes for SHA256) |
 * +------------+--------------------+-------------------------------+
 * */

/* we'll use hmac with sha256, which produces 32 byte output */
#define HM_LEN 32
#define KDF_KEY "qVHqkOVJLb7EolR9dsAMVwH1hRCYVx#I"
/* need to make sure KDF is orthogonal to other hash functions, like
 * the one used in the KDF, so we use hmac with a key. */

int ske_keyGen(SKE_KEY* K, unsigned char* entropy, size_t entLen)
{
	/* TODO: write this.  If entropy is given, apply a KDF to it to get
	 * the keys (something like HMAC-SHA512 with KDF_KEY will work).
	 * If entropy is null, just get a random key (you can use the PRF). */
	 unsigned char *key;
	 int key_len;
	 if(entropy == NULL)
	 {
		 randomBytes(entropy,64);
	 }
	 HMAC(EVP_sha512(),KDF_KEY,32,entropy, 64, key, key_len);
	 for(int i = 0; i < 32; i++)
	 {
		 K->hmacKey[i] = key[i];
		 K->aesKey[i] = key[i+32];
	 }
	return 0;
}
size_t ske_getOutputLen(size_t inputLen)
{
	return AES_BLOCK_SIZE + inputLen + HM_LEN;
}
size_t ske_encrypt(unsigned char* outBuf, unsigned char* inBuf, size_t len,
		SKE_KEY* K, unsigned char* IV)
{
	/* TODO: finish writing this.  Look at ctr_example() in aes-example.c
	 * for a hint.  Also, be sure to setup a random IV if none was given.
	 * You can assume outBuf has enough space for the result. */
	 unsigned char *ct;
	 unsigned char *y;
	 /*encrypt*/
	 if(IV == NULL)
	 {
		 randomBytes(IV, 16);
	 }
	EVP_CIPHER_CTX* ctx = EVP_CIPHER_CTX_new();
	if (1!=EVP_EncryptInit_ex(ctx,EVP_aes_256_ctr(),0,K->aesKey,IV))
		ERR_print_errors_fp(stderr);
	int nWritten;
	if (1!=EVP_EncryptUpdate(ctx,ct,&nWritten,inBuf,len))
		ERR_print_errors_fp(stderr);
	EVP_CIPHER_CTX_free(ctx);
	int ct_len;
	ct_len = strlen(ct);
	
	/*HMAC*/
	int y_len;
	HMAC(EVP_sha256(),K->hmacKey,32, ct, ct_len
				,y,y_len);
	strcat(outBuf, IV);
	strcat(outBuf, ct);
	strcat(outBuf, y);
	return strlen(ct); /* TODO: should return number of bytes written, which
	             hopefully matches ske_getOutputLen(...). */
}
size_t ske_encrypt_file(const char* fnout, const char* fnin,
		SKE_KEY* K, unsigned char* IV, size_t offset_out)
{
	/* TODO: write this.  Hint: mmap. */
	FILE *fp1,*fp2;
	unsigned char *message;
	unsigned char *cipher;
	fp1=fopen("fnin","r");
	fseek(fp1,0,SEEK_END);
	int input_len;
	input_len = ftell(fp1);
	rewind(fp1);
	fread(message, 1, input_len, fp1);
	fclose(fp1);
	ske_encrypt(cipher, message, input_len, K, IV);
	fp2=fopen("fnout","w");
	int cp_len;
	cp_len = strlen(cipher);
	fwrite(cipher, 1, cp_len, fp2);
	fclose(fp2);
	return 0;
}
size_t ske_decrypt(unsigned char* outBuf, unsigned char* inBuf, size_t len,
		SKE_KEY* K)
{
	/* TODO: write this.  Make sure you check the mac before decypting!
	 * Oh, and also, return -1 if the ciphertext is found invalid.
	 * Otherwise, return the number of bytes written.  See aes-example.c
	 * for how to do basic decryption. */
	 unsigned char *pt;
	 unsigned char *ct;
	 unsigned char *IV;
	 unsigned char *y;
	 unsigned char *y_check;
	 int y_len, y_check_len;
	 int ct_len;
	 ct_len = strlen(inBuf) - 16 - 32;
	 for(int i = 0; i < 16; i++)
	 {
		 IV[i] = inBuf[i];
	 }
	 for(int i = 0; i < ct_len;i++)
	 {
		 ct[i] = inBuf[i+16];
	 }
	 for(int i = 0; i < 32; i++)
	 {
		 y[i] = inBuf[i+16+ct_len];
	 }
	 HMAC(EVP_sha256(), k->hmacKey, 32, ct, ct_len, y_check, y_check_len);
	 if(y_check != y)
	 {
		 return -1;
	 }
	 int nWritten;
	 nWritten = 0;
	ctx = EVP_CIPHER_CTX_new();
	if (1!=EVP_DecryptInit_ex(ctx,EVP_aes_256_ctr(),0,K->aesKey,IV))
		ERR_print_errors_fp(stderr);
	if (1!=EVP_DecryptUpdate(ctx,pt,&nWritten,ct,ctLen))
		ERR_print_errors_fp(stderr);
	outBuf = pt;
	return 0;
}
size_t ske_decrypt_file(const char* fnout, const char* fnin,
		SKE_KEY* K, size_t offset_in)
{
	/* TODO: write this. */
	FILE *fp1, *fp2;
	unsigned char *cipher;
	unsigned char *message;
	fp1=fopen("fnin","r");
	fseek(fp1,0,SEEK_END);
	int input_len;
	input_len = ftell(fp1);
	rewind(fp1);
	fread(cipher, 1, input_len, fp1);
	fclose(fp1);
	ske_decrypt(message, cipher, input_len, K);
	fp2=fopen("fnout","w");
	int m_len;
	m_len = strlen(message);
	fwrite(message, 1, m_len, fp2);
	fclose(fp2);
	return 0;
}
